This little script will help in getting an authenticated session
into an AVI Networks Vantage controller. It then allows you to easily GET,
POST or PUT data to the controller without having to re-authenticate each time.

It will collect the cookies and CSRFTOKEN required for the session.
It gives examples to use with GET and POST.

It creates an AVI_LOGOUT script to run when finished. This will clean-up the cookie file
and will log you out to avoid security incidents.

Usage: ./aviLogin.sh
